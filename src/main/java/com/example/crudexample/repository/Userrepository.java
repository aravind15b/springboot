package com.example.crudexample.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.crudexample.model.User;
@Repository
public interface Userrepository extends JpaRepository<User,Long> {

}
