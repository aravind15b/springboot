package com.example.crudexample.model;

import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "Ghost")
public class User {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private Long Id;
@Column
@NotNull
@NotEmpty
private String Emailaddress;
@Column
@NotNull
@NotEmpty
private String Username;
@Column
@NotNull
@NotEmpty
private String Password;
@Column
@CreationTimestamp
@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy")
private Date CreatedOn;
@Column
@UpdateTimestamp
@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy")
private Date UpdatedOn;
public Long getId() {
	return Id;
}
public void setId(Long id) {
	Id = id;
}
public String getEmailaddress() {
	return Emailaddress;
}
public void setEmailaddress(String emailaddress) {
	Emailaddress = emailaddress;
}
public String getUsername() {
	return Username;
}
public void setUsername(String username) {
	Username = username;
}
public String getPassword() {
	return Password;
}
public void setPassword(String password) {
	Password = password;
}
public Date getCreatedOn() {
	return CreatedOn;
}
public void setCreatedOn(Date createdOn) {
	CreatedOn = createdOn;
}
public Date getUpdatedOn() {
	return UpdatedOn;
}
public void setUpdatedOn(Date updatedOn) {
	UpdatedOn = updatedOn;
}
@Override
public String toString() {
	return "User [Id=" + Id + ", Emailaddress=" + Emailaddress + ", Username=" + Username + ", Password=" + Password
			+ ", CreatedOn=" + CreatedOn + ", UpdatedOn=" + UpdatedOn + "]";
}

}



