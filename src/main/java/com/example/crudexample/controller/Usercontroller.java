package com.example.crudexample.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.crudexample.model.User;
import com.example.crudexample.repository.Userrepository;

@RequestMapping("/api/Register")
@RestController
public class Usercontroller {
private static final Logger LOGGER = LoggerFactory.getLogger(Usercontroller.class);
@Autowired	
private Userrepository userrepository;


@GetMapping
public List<User> ShowRegister()
{
	return userrepository.findAll();
}
@PostMapping

public void InsertRegister(@RequestBody @Valid User user,BindingResult bindingResult) 
{
	if(bindingResult.hasErrors()) {
        bindingResult.getAllErrors().forEach(err -> {
            LOGGER.info("ERROR {}", err.getDefaultMessage());
        });
	}
	System.out.println("Name of Register:"+ user.getUsername());
	userrepository.save(user);
}


@PutMapping("/{id}")
public void UpdateRegister(@RequestBody User register,@PathVariable("id") long id) 
{
		register.setId(id);
		userrepository.save(register);		
}
@DeleteMapping("/{id}")
public void DeleteRegister(@PathVariable("id") long id) 
{
	 userrepository.deleteById(id);

}

}
